package br.com.app.core.repository;

import br.com.app.core.model.Bandeira;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BandeiraRepository extends JpaRepository<Bandeira, Long>{

	@Query("select a from Bandeira a where a.ativo = true")
	List<Bandeira> findAllAtivo();

    Bandeira findByNome(String nome);

	Bandeira findBandeiraById(Long id);

	void deleteById(Long id);
}
