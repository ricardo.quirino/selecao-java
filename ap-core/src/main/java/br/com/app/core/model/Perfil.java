package br.com.app.core.model;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity(name="Perfil")
public class Perfil extends GenericModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue
	@Column(name="Id_Perfil")
	private Long id;

	@Column(name="nome")
	private String nome;

	@Column(name="ativo")
	private Boolean ativo;


	public Perfil() {}

	public Perfil(String nome, Boolean ativo) {
		this.nome = nome;
		this.ativo = ativo;
	}
}
