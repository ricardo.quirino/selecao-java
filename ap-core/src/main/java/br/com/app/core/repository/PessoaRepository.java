package br.com.app.core.repository;

import java.util.List;

import br.com.app.core.model.Distribuidora;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.app.core.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long>{

	
	@Query("select p from Pessoa p where p.ativo = true")
	List<Pessoa> findAllAtivo();

	Pessoa findDistinctById(Long id);

	void deleteById(Long id);
}
