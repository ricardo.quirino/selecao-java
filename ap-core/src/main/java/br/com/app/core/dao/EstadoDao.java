package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Estado;
import br.com.app.core.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoDao extends GenericDao{

	@Autowired
	EstadoRepository estadoRepository;

	@Autowired
	Run configuracao;

	public void inserir(Estado estado){

		estadoRepository.save(estado);
	}

	public List<Estado> listar(){

		return estadoRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		estadoRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Estado estado) {

		estadoRepository.save(estado);
		return false;
	}

	public Estado buscarPorId(Long id) {

		Estado estado = estadoRepository.findDistinctById(id);
		return estado;
	}

    public Estado buscarPorNome(String nome) {
		Estado estado = estadoRepository.findByNome(nome);
		return estado;
    }
}
