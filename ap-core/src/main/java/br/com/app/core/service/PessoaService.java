package br.com.app.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.app.core.dao.PessoaDao;
import br.com.app.core.model.Pessoa;

@Service
public class PessoaService  extends GenericService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	PessoaDao pessoaDao;
	
	
	public void inserir(Pessoa pessoa){
		pessoaDao.inserir(pessoa);
	}

	public List<Pessoa> listar() {
		return pessoaDao.listar();
	}

	public boolean excluir(Long id) {
		
		Pessoa pessoa = pessoaDao.buscarPorId(id);
		
		pessoa.setAtivo(false);
		pessoaDao.alterar(pessoa);
		return true;
	}

	public boolean alterar(Pessoa pessoa) {
		pessoaDao.alterar(pessoa);
		return false;
	}

	public Pessoa buscarPorId(Long id) {
		
		return pessoaDao.buscarPorId(id);
	}
}
