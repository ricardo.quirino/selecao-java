package br.com.app.core.service;

import br.com.app.core.dao.DistribuidoraDao;
import br.com.app.core.model.Distribuidora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistribuidoraService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final GenericService genericService = new GenericService();

	@Autowired
	DistribuidoraDao distribuidoraDao;

	public void inserir(Distribuidora distribuidora){

		distribuidoraDao.inserir(distribuidora);
	}

	public List<Distribuidora> listar() {
		return distribuidoraDao.listar();
	}

	public boolean excluir(Long id) {
		Distribuidora distribuidora = distribuidoraDao.buscarPorId(id);
		distribuidora.setAtivo(false);
		distribuidoraDao.alterar(distribuidora);
		return true;
	}

	public boolean alterar(Distribuidora distribuidora) {
		distribuidoraDao.alterar(distribuidora);
		return false;
	}

	public Distribuidora buscarPorId(Long id) {
		return distribuidoraDao.buscarPorId(id);
	}


    public Distribuidora buscarPorCodigo(String codigo) {
		return distribuidoraDao.buscarPorCodigo(codigo);
    }
}
