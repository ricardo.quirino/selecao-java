package br.com.app.core.response;

import br.com.app.core.model.ModelEntity;
import br.com.app.core.model.Municipio;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MediaPrecoMunicipioResponse implements Serializable {

  private String municipio;
  private String estado;
  private Double mediaPrecoVenda;

  public MediaPrecoMunicipioResponse() {}


  public MediaPrecoMunicipioResponse(String municipio, String estado, Double mediaPrecoVenda) {
    this.municipio = municipio;
    this.estado = estado;
    this.mediaPrecoVenda = mediaPrecoVenda;
  }
}
