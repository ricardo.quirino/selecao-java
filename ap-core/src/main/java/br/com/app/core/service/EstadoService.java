package br.com.app.core.service;

import br.com.app.core.dao.EstadoDao;
import br.com.app.core.model.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final GenericService genericService = new GenericService();

	@Autowired
	EstadoDao estadoDao;

	public void inserir(Estado estado){

		estadoDao.inserir(estado);
	}

	public List<Estado> listar() {
		return estadoDao.listar();
	}

	public boolean excluir(Long id) {
		Estado estado = estadoDao.buscarPorId(id);
		estado.setAtivo(false);
		estadoDao.alterar(estado);
		return true;
	}

	public boolean alterar(Estado estado) {
		estadoDao.alterar(estado);
		return false;
	}

	public Estado buscarPorId(Long id) {
		return estadoDao.buscarPorId(id);
	}

    public Estado buscarPorNome(String nome) {
			return estadoDao.buscarPorNome(nome);
    }
}
