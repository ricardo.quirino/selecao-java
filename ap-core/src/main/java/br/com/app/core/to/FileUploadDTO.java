package br.com.app.core.to;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class FileUploadDTO {



    private String regiao;
    private String siglaEstado;
    private String municipio;
    private String distribuidora;
    private String codigo;
    private String produto;
    private String dataColeta;
    private Double valorCompra;
    private Double valorVenda;
    private String unidadeMedida;
    private String bandeira;

    public FileUploadDTO(String linhas[], String msg) {


        regiao            = linhas[0];
        siglaEstado		  = linhas[1];
        municipio	      = linhas[2];
        distribuidora	  = linhas[3].replace(".", "");
        codigo			  = linhas[4];
        produto			  = linhas[5];
        dataColeta		  = linhas[6];
        valorCompra       = formatarValor(linhas[7].replace(",", ".").replace("R$", "").replace("/",""),msg);
        valorVenda        = formatarValor(linhas[8].replace(",", ".").replace("R$", "").replace("/",""), msg);
        unidadeMedida	  = linhas[9].replace("R$", "").replace("/","").trim();

        if(linhas.length >10)
        bandeira		  = linhas[10].replace(".", "");

    }


    public Double formatarValor(String valorCompra, String msg) {

        try {
            if (valorCompra != null
                    && !valorCompra.isEmpty()
                    && !valorCompra.contains("/")
                    && valorCompra.substring(0,1).matches("[0-9]*")) {

                return new Double(valorCompra);

            }
             else {
                return 0.00;
            }


        } catch (Exception e) {
            return 0.00;
        }


    }









    @Override
    public String toString() {
        return "FileUploadDTO{" +
                "regiao='" + regiao + '\'' +
                ", siglaEstado='" + siglaEstado + '\'' +
                ", municipio='" + municipio + '\'' +
                ", distribuidora='" + distribuidora + '\'' +
                ", codigo='" + codigo + '\'' +
                ", produto='" + produto + '\'' +
                ", dataColeta='" + dataColeta + '\'' +
                ", valorCompra='" + valorCompra + '\'' +
                ", valorVendo='" + valorVenda + '\'' +
                ", unidadeMedida='" + unidadeMedida + '\'' +
                ", bandeira='" + bandeira + '\'' +
                '}';
    }
}
