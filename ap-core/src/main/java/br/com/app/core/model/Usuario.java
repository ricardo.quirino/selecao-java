package br.com.app.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name="Usuario")
public class Usuario extends GenericModel{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue
	@Column(name="id_Usuario")
	private Long id;
	
	@Column(name="ativo")
	private Boolean ativo;
	
	@Column(name="login")
	private String login;
	
	@Column(name="senha")
	private String senha;

	@OneToOne
	@JoinColumn(name="id_pessoa")
	private Pessoa pessoa;

	@ManyToOne
	@JoinColumn(name="id_Perfil")
	private Perfil perfil;
}
