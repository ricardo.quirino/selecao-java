package br.com.app.core.controller;

import java.util.ArrayList;
import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.app.core.model.Pessoa;
import br.com.app.core.service.PessoaService;

@CrossOrigin
@RestController
@Api(value = "Pessoa",  description = "API REST Pessoa")
@RequestMapping("/pessoa")
public class PessoaController implements GenericController {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	PessoaService pessoaService;
	
	
	
	//INSERT POST
	@ApiOperation(value = "Insere uma nova pessoa")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pessoa> inserir(@RequestBody Pessoa pessoa){
				
		pessoaService.inserir(pessoa);
		return new ResponseEntity<Pessoa>(pessoa, HttpStatus.CREATED);
	}
	
	//LISTAR GET
	@ApiOperation(value = "Retorna tadas as pessoas Ativas")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pessoa>> listar(){
		
		List<Pessoa> listaConvidados = new ArrayList<>();
		listaConvidados = pessoaService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna a pessoa pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Pessoa> buscarPorId(@PathVariable("id") long id) {   

		Pessoa pessoa = new Pessoa();
		pessoa = pessoaService.buscarPorId(id);
		return new ResponseEntity<>(pessoa, HttpStatus.OK);
	}
	
    
	//ALTERAR PUT
	@ApiOperation(value = "Altera os dados cadastrais da pessoa")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pessoa> alterar(@RequestBody Pessoa pessoa){
		
		pessoaService.alterar(pessoa);
		return new ResponseEntity<>(pessoa, HttpStatus.CREATED);
	}
	
	
	
	//EXCLUIR PUT
	@ApiOperation(value = "Exclui uma pessoa")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pessoa> excluir(@RequestBody Pessoa pessoa){		
		
		pessoaService.excluir(pessoa.getId());
		return new ResponseEntity<>(pessoa, HttpStatus.CREATED);
	}



}
