package br.com.app.core.controller;

import br.com.app.core.model.HistoricoPrecoCombustivel;
import br.com.app.core.response.*;
import br.com.app.core.service.HistoricoPrecoCombustivelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "HistoricoPrecoCombustivel",  description = "API REST HistoricoPrecoCombustivel")
@RequestMapping("/historicoprecocombustivel")
public class HistoricoPrecoCombustivelController implements GenericController {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	HistoricoPrecoCombustivelService historicoPrecoCombustivelService;


	@ApiOperation(value = "Insere um novo historicoprecocombustivel")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HistoricoPrecoCombustivel> inserir(@RequestBody HistoricoPrecoCombustivel historicoprecocombustivel){

		historicoPrecoCombustivelService.inserir(historicoprecocombustivel);
		return new ResponseEntity<HistoricoPrecoCombustivel>(historicoprecocombustivel, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Insere importacão de arquivo xlsx e csv")
	@PostMapping("/upload")
	public ResponseEntity singleFileUpload(@RequestParam("file") MultipartFile fileupload, RedirectAttributes redirectAttributes) {

		if (!fileupload.isEmpty()) {
			historicoPrecoCombustivelService.inserirLote(fileupload);
			return new ResponseEntity<>(HttpStatus.OK);
		}else{
			redirectAttributes.addFlashAttribute("Eroo", "For favor adicionar um arquivo");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "Retorna todos os historicoprecocombustivels")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HistoricoPrecoCombustivel>> listar(){

		List<HistoricoPrecoCombustivel> listaConvidados = new ArrayList<>();
		listaConvidados = historicoPrecoCombustivelService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna o historicoprecocombustivel pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<HistoricoPrecoCombustivel> buscarPorId(@PathVariable("id") long id) {

		HistoricoPrecoCombustivel historicoprecocombustivel = new HistoricoPrecoCombustivel();
		historicoprecocombustivel = historicoPrecoCombustivelService.buscarPorId(id);
		return new ResponseEntity<>(historicoprecocombustivel, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais do historicoprecocombustivel")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HistoricoPrecoCombustivel> alterar(@RequestBody HistoricoPrecoCombustivel historicoprecocombustivel){

		historicoPrecoCombustivelService.alterar(historicoprecocombustivel);
		return new ResponseEntity<>(historicoprecocombustivel, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui um historicoprecocombustivel")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HistoricoPrecoCombustivel> excluir(@RequestBody HistoricoPrecoCombustivel historicoprecocombustivel){

		historicoPrecoCombustivelService.excluir(historicoprecocombustivel.getId());
		return new ResponseEntity<>(historicoprecocombustivel, HttpStatus.CREATED);
	}


	@ApiOperation(value = "Retorna a media de preço de combustivel por regiao")
	@RequestMapping(value="/media-preco", method = RequestMethod.GET)
	public ResponseEntity<List<MediaPrecoRegiaoResponse>> mediaPrecoVenda() {

		List<MediaPrecoRegiaoResponse> response = historicoPrecoCombustivelService.mediaPrecoPorRegiao();
		if(response == null){
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		}else{
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

	}

	@ApiOperation(value = "Retorna a media de preço de combustivel no {municipio}")
	@RequestMapping(value="media-preco/{municipio}", method = RequestMethod.GET)
	public ResponseEntity<List<MediaPrecoMunicipioResponse>> mediaPrecoVenda(@PathVariable("municipio") String municipio) {

		List<MediaPrecoMunicipioResponse> response = historicoPrecoCombustivelService.mediaPrecoVenda(municipio);

		if(response == null){
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		}else{
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}

	@ApiOperation(value = "Retorna a media de preço de combustivel por distribuidora e regiao")
	@RequestMapping(value="/media-preco-distribuidora", method = RequestMethod.GET)
	public ResponseEntity<List<MediaPrecoDistribuidoraResponse>> mediaPrecoVendaDistribuidora() {


		List<MediaPrecoDistribuidoraResponse> response = historicoPrecoCombustivelService.mediaPrecoVendaDistribuidora();
		if(response == null){
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		}else{
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

	}

	@ApiOperation(value = "Retorna a media de preço por data da coleta")
	@RequestMapping(value="/media-preco-data-coleta", method = RequestMethod.GET)
	public ResponseEntity<List<MediaPrecoDataColetaResponse>> mediaPrecoDataColeta() {


		List<MediaPrecoDataColetaResponse> response = historicoPrecoCombustivelService.mediaPrecoDataColeta();
		if(response == null){
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		}else{
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

	}


	@ApiOperation(value = "Retorna a media de preço de combustivel Municipio")
	@RequestMapping(value="/media-preco-municipio", method = RequestMethod.GET)
	public ResponseEntity<List<MediaPrecoMunicipioResponse>> listaMediaPrecoMunicipio() {

		List<MediaPrecoMunicipioResponse> response = historicoPrecoCombustivelService.listaMediaPrecoMunicipio();
		if(response == null){
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		}else{
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

	}


	@ApiOperation(value = "Retorna a media de preço de combustivel por bandeira {bandeira}")
	@RequestMapping(value="media-preco-bandeira/{bandeira}", method = RequestMethod.GET)
	public ResponseEntity<List<MediaPrecoBandeiraResponse>> mediaPrecoBandeira(@PathVariable("bandeira") String bandeira) {

		List<MediaPrecoBandeiraResponse> response = historicoPrecoCombustivelService.mediaPrecoBandeira(bandeira);

		if(response == null){
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		}else{
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}



}
