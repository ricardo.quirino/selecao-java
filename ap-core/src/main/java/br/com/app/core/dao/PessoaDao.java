package br.com.app.core.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.app.core.Run;
import br.com.app.core.model.Pessoa;
import br.com.app.core.repository.PessoaRepository;

@Service
public class PessoaDao extends GenericDao{

	@Autowired
	PessoaRepository pessoaRepository;
	@Autowired
	Run configuracao;
	
	public void inserir(Pessoa pessoa){
		
		pessoaRepository.save(pessoa);
	}
	
	public List<Pessoa> listar(){
		
		return pessoaRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		pessoaRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Pessoa pessoa) {
		
		pessoaRepository.save(pessoa);
		return false;
	}
	
	public Pessoa buscarPorId(Long id) {
		
		Pessoa pessoa = pessoaRepository.findDistinctById(id);
		return pessoa;
	}
	
}
