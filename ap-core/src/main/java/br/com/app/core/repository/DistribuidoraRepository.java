package br.com.app.core.repository;

import br.com.app.core.model.Distribuidora;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DistribuidoraRepository extends JpaRepository<Distribuidora, Long>{

	@Query("select a from Distribuidora a where a.ativo = true")
	List<Distribuidora> findAllAtivo();

    Distribuidora findByCodigo(String codigo);

	Distribuidora findDistinctById(Long id);

	void deleteById(Long id);
}
