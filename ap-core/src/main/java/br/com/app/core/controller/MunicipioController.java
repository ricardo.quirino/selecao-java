package br.com.app.core.controller;

import br.com.app.core.model.Municipio;
import br.com.app.core.service.MunicipioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "Municipio",  description = "API REST Municipio")
@RequestMapping("/municipio")
public class MunicipioController implements GenericController {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	MunicipioService municipioService;


	@ApiOperation(value = "Insere um novo municipio")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Municipio> inserir(@RequestBody Municipio municipio){

		municipioService.inserir(municipio);
		return new ResponseEntity<Municipio>(municipio, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna todos os municipios")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Municipio>> listar(){

		List<Municipio> listaConvidados = new ArrayList<>();
		listaConvidados = municipioService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna o municipio pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Municipio> buscarPorId(@PathVariable("id") long id) {

		Municipio municipio = new Municipio();
		municipio = municipioService.buscarPorId(id);
		return new ResponseEntity<>(municipio, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais do municipio")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Municipio> alterar(@RequestBody Municipio municipio){

		municipioService.alterar(municipio);
		return new ResponseEntity<>(municipio, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui um municipio")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Municipio> excluir(@RequestBody Municipio municipio){

		municipioService.excluir(municipio.getId());
		return new ResponseEntity<>(municipio, HttpStatus.CREATED);
	}


}
