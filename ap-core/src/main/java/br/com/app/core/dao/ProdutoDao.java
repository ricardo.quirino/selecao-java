package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Produto;
import br.com.app.core.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdutoDao extends GenericDao{

	@Autowired
	ProdutoRepository produtoRepository;

	@Autowired
	Run configuracao;

	public void inserir(Produto produto){

		produtoRepository.save(produto);
	}

	public List<Produto> listar(){

		return produtoRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		produtoRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Produto produto) {

		produtoRepository.save(produto);
		return false;
	}

	public Produto buscarPorId(Long id) {

		Produto produto = produtoRepository.findDistinctById(id);
		return produto;
	}

    public Produto buscarPorNome(String nome) {

		Produto produto = produtoRepository.findByNome(nome);
		return produto;
    }
}
