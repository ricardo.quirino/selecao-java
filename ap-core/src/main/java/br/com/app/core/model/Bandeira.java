package br.com.app.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity(name="Bandeira")
public class Bandeira extends GenericModel{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="id_Bandeira")
	private Long id;

	@Column(name="Nome")
	private String nome;

	@Column(name="ativo")
	private Boolean ativo;

}
