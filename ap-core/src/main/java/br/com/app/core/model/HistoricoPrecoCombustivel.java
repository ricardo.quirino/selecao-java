package br.com.app.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity(name="HistoricoPrecoCombustivel")
public class HistoricoPrecoCombustivel extends GenericModel{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="id_HistoricoPrecoCombustivel")
	private Long id;

	@Column(name="dataColeta")
	private String dataColeta;

	@Column(name="valorCompra")
	private Double valorCompra;

	@Column(name="valorVenda")
	private Double valorVenda;

	@Column(name="unidadeMedida")
	private String unidadeMedida;

	@Column(name="ativo")
	private Boolean ativo;


	//


	@ManyToOne
	@JoinColumn(name="id_Distribuidora")
	private Distribuidora distribuidora;

	@ManyToOne
	@JoinColumn(name="id_Produto")
	private Produto produto;

	@ManyToOne
	@JoinColumn(name="id_Municipio")
	private Municipio municipio;



}
