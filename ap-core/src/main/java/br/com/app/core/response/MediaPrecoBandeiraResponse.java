package br.com.app.core.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaPrecoBandeiraResponse {


    private String bandeira;
    private Double mediaCompra;
    private Double mediaPrecoVenda;

    public MediaPrecoBandeiraResponse() {
    }

    public MediaPrecoBandeiraResponse(String bandeira, Double mediaCompra, Double mediaPrecoVenda) {
        this.bandeira = bandeira;
        this.mediaCompra = mediaCompra;
        this.mediaPrecoVenda = mediaPrecoVenda;
    }
}
