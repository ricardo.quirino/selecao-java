package br.com.app.core.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaPrecoRegiaoResponse {


    private String regiao;
    private Double mediaCompra;
    private Double mediaPrecoVenda;

    public MediaPrecoRegiaoResponse() {
    }

    public MediaPrecoRegiaoResponse(String nome, Double mediaCompra, Double mediaPrecoVenda) {
        this.regiao = nome;
        this.mediaCompra = mediaCompra;
        this.mediaPrecoVenda = mediaPrecoVenda;
    }
}
