package br.com.app.core.config;

import br.com.app.core.model.Perfil;
import br.com.app.core.model.Pessoa;
import br.com.app.core.model.Usuario;
import br.com.app.core.service.PerfilService;
import br.com.app.core.service.PessoaService;
import br.com.app.core.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfiguracaoAcesso {

    @Autowired
    UsuarioService usuarioService;
    @Autowired
    PessoaService pessoaService;
    @Autowired
    PerfilService perfilService;

    @Bean
    public ConfiguracaoAcesso anotherComponent() {

        Usuario usuario = new Usuario();
        Perfil perfil = new Perfil("ADMIN", true);
        Pessoa pessoa = new Pessoa("João", "Quirino", "joao quirino", true);

        usuario.setLogin("user.admin");
        usuario.setSenha("1234");
        usuario.setPessoa(pessoa);
        usuario.setPerfil(perfil);
        usuario.setAtivo(true);

        pessoaService.inserir(pessoa);
        perfilService.inserir(perfil);
        usuarioService.inserir(usuario);

        return this;
    }





}
