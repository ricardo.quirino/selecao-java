package br.com.app.core.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MediaPrecoDataColetaResponse implements Serializable {

  private String dataColeta;
  private Double mediaPrecoCompra;
  private Double mediaPrecoVenda;

  public MediaPrecoDataColetaResponse() {}


  public MediaPrecoDataColetaResponse(String dataColeta, Double mediaPrecoCompra, Double mediaPrecoVenda) {
    this.dataColeta = dataColeta;
    this.mediaPrecoCompra = mediaPrecoCompra;
    this.mediaPrecoVenda = mediaPrecoVenda;
  }
}
