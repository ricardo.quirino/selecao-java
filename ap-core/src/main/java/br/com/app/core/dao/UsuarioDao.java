package br.com.app.core.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.app.core.Run;
import br.com.app.core.model.Usuario;
import br.com.app.core.repository.UsuarioRepository;

@Service
public class UsuarioDao extends GenericDao{

	@Autowired
	UsuarioRepository usuarioRepository;
	@Autowired
	Run configuracao;
	
	public void inserir(Usuario usuario){
		
		usuarioRepository.save(usuario);
	}
	
	public List<Usuario> listar(){
		
		return usuarioRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		usuarioRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Usuario usuario) {
		
		usuarioRepository.save(usuario);
		return false;
	}
	
	public Usuario buscarPorId(Long id) {
		return usuarioRepository.findUsuarioById(id);
	}

	public Usuario buscarPorLogin(String login) {
		return usuarioRepository.findUsuarioByLogin(login);
	}

	public Usuario buscarPorIdPessoa(Long id) {
		return usuarioRepository.findByIdPessoa(id);
	}

	public Usuario buscarPorLoginSenha(String login, String senha) {
		return usuarioRepository.findByLoginAndSenha(login, senha);
	}
	
}
