package br.com.app.core.controller;

import br.com.app.core.model.Bandeira;
import br.com.app.core.service.BandeiraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "Bandeira",  description = "API REST Bandeira")
@RequestMapping("/bandeira")
public class BandeiraController implements GenericController {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	BandeiraService bandeiraService;


	@ApiOperation(value = "Insere uma nova bandeira")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Bandeira> inserir(@RequestBody Bandeira bandeira){

		bandeiraService.inserir(bandeira);
		return new ResponseEntity<Bandeira>(bandeira, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna tadas as bandeiras")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Bandeira>> listar(){

		List<Bandeira> listaConvidados = new ArrayList<>();
		listaConvidados = bandeiraService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna a bandeira pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Bandeira> buscarPorId(@PathVariable("id") long id) {

		Bandeira bandeira = new Bandeira();
		bandeira = bandeiraService.buscarPorId(id);
		return new ResponseEntity<>(bandeira, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais da bandeira")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Bandeira> alterar(@RequestBody Bandeira bandeira){

		bandeiraService.alterar(bandeira);
		return new ResponseEntity<>(bandeira, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui uma bandeira")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Bandeira> excluir(@RequestBody Bandeira bandeira){

		bandeiraService.excluir(bandeira.getId());
		return new ResponseEntity<>(bandeira, HttpStatus.CREATED);
	}


}
