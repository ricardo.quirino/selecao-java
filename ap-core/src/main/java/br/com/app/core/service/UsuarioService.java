package br.com.app.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.app.core.dao.UsuarioDao;
import br.com.app.core.model.Usuario;
import br.com.app.core.util.CriptografiaUtil;

@Service
public class UsuarioService  extends GenericService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	UsuarioDao usuarioDao;
	
	public void inserir(Usuario usuario){
				
		usuario.setSenha(CriptografiaUtil.criptografar(usuario.getSenha()));
		usuarioDao.inserir(usuario);
	}

	public List<Usuario> listar() {
		return usuarioDao.listar();
	}

	public boolean excluir(Long id) {
		
		Usuario usuario = usuarioDao.buscarPorId(id);
		
		usuario.setAtivo(false);
		usuarioDao.alterar(usuario);
		return true;
	}

	public boolean alterar(Usuario usuario) {
		usuarioDao.alterar(usuario);
		return false;
	}

	public Usuario buscarPorId(Long id) {
		
		return usuarioDao.buscarPorId(id);
	}
	
	public Usuario buscarPorIdPessoa(Long id) {
		
		return usuarioDao.buscarPorIdPessoa(id);
	}

	public Usuario buscarPorLoginSenha(String login, String senha) {
		
		senha = CriptografiaUtil.criptografar(senha);
		Usuario usuarioLogin = usuarioDao.buscarPorLogin(login);

		if(usuarioLogin != null && usuarioLogin.getSenha().equals(senha)){
			return usuarioLogin;
		}else{
			return null;
		}


	
}
	
}
