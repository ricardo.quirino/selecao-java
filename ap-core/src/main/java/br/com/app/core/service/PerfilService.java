package br.com.app.core.service;

import br.com.app.core.dao.PerfilDao;
import br.com.app.core.model.Perfil;
import br.com.app.core.util.CriptografiaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerfilService extends GenericService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	PerfilDao perfilDao;
	
	public void inserir(Perfil perfil){

		perfilDao.inserir(perfil);
	}

	public List<Perfil> listar() {
		return perfilDao.listar();
	}

	public boolean excluir(Long id) {

		Perfil perfil = perfilDao.buscarPorId(id);

		perfil.setAtivo(false);
		perfilDao.alterar(perfil);
		return true;
	}

	public boolean alterar(Perfil perfil) {
		perfilDao.alterar(perfil);
		return false;
	}

	public Perfil buscarPorId(Long id) {
		
		return perfilDao.buscarPorId(id);
	}
	
}
