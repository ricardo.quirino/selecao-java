package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Municipio;
import br.com.app.core.repository.MunicipioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MunicipioDao extends GenericDao{

	@Autowired
	MunicipioRepository municipioRepository;

	@Autowired
	Run configuracao;

	public void inserir(Municipio municipio){

		municipioRepository.save(municipio);
	}

	public List<Municipio> listar(){

		return municipioRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		municipioRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Municipio municipio) {

		municipioRepository.save(municipio);
		return false;
	}

	public Municipio buscarPorId(Long id) {

		Municipio municipio = municipioRepository.findDistinctById(id);
		return municipio;
	}

    public Municipio buscarPorNome(String nome) {
		Municipio municipio = municipioRepository.findByNome(nome);
		return municipio;
    }
}
