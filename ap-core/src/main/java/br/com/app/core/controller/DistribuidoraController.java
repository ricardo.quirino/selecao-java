package br.com.app.core.controller;

import br.com.app.core.model.Distribuidora;
import br.com.app.core.service.DistribuidoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "Distribuidora",  description = "API REST Distribuidora")
@RequestMapping("/distribuidora")
public class DistribuidoraController implements GenericController {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	DistribuidoraService distribuidoraService;


	@ApiOperation(value = "Insere uma nova distribuidora")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Distribuidora> inserir(@RequestBody Distribuidora distribuidora){

		distribuidoraService.inserir(distribuidora);
		return new ResponseEntity<Distribuidora>(distribuidora, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna tadas as distribuidoras")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Distribuidora>> listar(){

		List<Distribuidora> listaConvidados = new ArrayList<>();
		listaConvidados = distribuidoraService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna a distribuidora pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Distribuidora> buscarPorId(@PathVariable("id") long id) {

		Distribuidora distribuidora = new Distribuidora();
		distribuidora = distribuidoraService.buscarPorId(id);
		return new ResponseEntity<>(distribuidora, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais da distribuidora")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Distribuidora> alterar(@RequestBody Distribuidora distribuidora){

		distribuidoraService.alterar(distribuidora);
		return new ResponseEntity<>(distribuidora, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui uma distribuidora")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Distribuidora> excluir(@RequestBody Distribuidora distribuidora){

		distribuidoraService.excluir(distribuidora.getId());
		return new ResponseEntity<>(distribuidora, HttpStatus.CREATED);
	}


}
