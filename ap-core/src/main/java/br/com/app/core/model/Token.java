package br.com.app.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Getter
@Setter
@Entity(name="Token")
public class Token extends GenericModel{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue
	@Column(name="Id_token")
	private Long id;
	
	@Column(name="token")
	private String token;
	
	@Column(name="ativo")
	private Boolean ativo;
}
