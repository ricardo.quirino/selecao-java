package br.com.app.core.service;

import br.com.app.core.dao.MunicipioDao;
import br.com.app.core.model.Municipio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MunicipioService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final GenericService genericService = new GenericService();

	@Autowired
	MunicipioDao municipioDao;

	public void inserir(Municipio municipio){

		municipioDao.inserir(municipio);
	}

	public List<Municipio> listar() {
		return municipioDao.listar();
	}

	public boolean excluir(Long id) {
		Municipio municipio = municipioDao.buscarPorId(id);
		municipio.setAtivo(false);
		municipioDao.alterar(municipio);
		return true;
	}

	public boolean alterar(Municipio municipio) {
		municipioDao.alterar(municipio);
		return false;
	}

	public Municipio buscarPorId(Long id) {
		return municipioDao.buscarPorId(id);
	}

    public Municipio buscarPorNome(String municipio) {
		return municipioDao.buscarPorNome(municipio);
    }
}
