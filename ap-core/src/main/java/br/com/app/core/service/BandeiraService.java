package br.com.app.core.service;

import br.com.app.core.dao.BandeiraDao;
import br.com.app.core.model.Bandeira;
import br.com.app.core.model.Regiao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BandeiraService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final GenericService genericService = new GenericService();

	@Autowired
	BandeiraDao bandeiraDao;

	public void inserir(Bandeira bandeira){

		bandeiraDao.inserir(bandeira);
	}

	public List<Bandeira> listar() {
		return bandeiraDao.listar();
	}

	public boolean excluir(Long id) {
		Bandeira bandeira = bandeiraDao.buscarPorId(id);
		bandeira.setAtivo(false);
		bandeiraDao.alterar(bandeira);
		return true;
	}

	public boolean alterar(Bandeira bandeira) {
		bandeiraDao.alterar(bandeira);
		return false;
	}

	public Bandeira buscarPorId(Long id) {
		return bandeiraDao.buscarPorId(id);
	}

    public Bandeira buscarPorNome(String nome) {
		return bandeiraDao.buscarPorNome(nome);
    }
}
