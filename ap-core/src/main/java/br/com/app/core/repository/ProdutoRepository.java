package br.com.app.core.repository;

import br.com.app.core.model.Distribuidora;
import br.com.app.core.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProdutoRepository extends JpaRepository<Produto, Long>{

	@Query("select a from Produto a where a.ativo = true")
	List<Produto> findAllAtivo();

    Produto findByNome(String nome);

	Produto findDistinctById(Long id);

	void deleteById(Long id);
}
