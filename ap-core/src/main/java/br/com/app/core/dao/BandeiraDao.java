package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Bandeira;
import br.com.app.core.model.Regiao;
import br.com.app.core.repository.BandeiraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BandeiraDao extends GenericDao{

	@Autowired
	BandeiraRepository bandeiraRepository;

	@Autowired
	Run configuracao;

	public void inserir(Bandeira bandeira){

		bandeiraRepository.save(bandeira);
	}

	public List<Bandeira> listar(){

		return bandeiraRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		bandeiraRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Bandeira bandeira) {

		bandeiraRepository.save(bandeira);
		return false;
	}

	public Bandeira buscarPorId(Long id) {

		Bandeira bandeira = bandeiraRepository.findBandeiraById(id);
		return bandeira;
	}


    public Bandeira buscarPorNome(String nome) {

		Bandeira bandeira = bandeiraRepository.findByNome(nome);
		return bandeira;
    }
}
