package br.com.app.core.repository;

import br.com.app.core.model.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MunicipioRepository extends JpaRepository<Municipio, Long>{

	@Query("select a from Municipio a where a.ativo = true")
	List<Municipio> findAllAtivo();

	Municipio findByNome(String login);

	Municipio findDistinctById(Long id);

	void deleteById(Long id);
}
