package br.com.app.core.service;

import br.com.app.core.dao.RegiaoDao;
import br.com.app.core.model.Regiao;
import br.com.app.core.response.MediaPrecoRegiaoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegiaoService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final GenericService genericService = new GenericService();

	@Autowired
	RegiaoDao regiaoDao;

	@Autowired
	HistoricoPrecoCombustivelService historicoPrecoCombustivelService;

	public void inserir(Regiao regiao){

		regiaoDao.inserir(regiao);
	}

	public List<Regiao> listar() {
		return regiaoDao.listar();
	}

	public boolean excluir(Long id) {
		Regiao regiao = regiaoDao.buscarPorId(id);
		regiao.setAtivo(false);
		regiaoDao.alterar(regiao);
		return true;
	}

	public boolean alterar(Regiao regiao) {
		regiaoDao.alterar(regiao);
		return false;
	}

	public Regiao buscarPorId(Long id) {
		return regiaoDao.buscarPorId(id);
	}

    public Regiao buscarPorNome(String nome) {
		return regiaoDao.buscarPorNome(nome);
    }

    public List<MediaPrecoRegiaoResponse> mediaPreco() {
		return historicoPrecoCombustivelService.mediaPrecoPorRegiao();
    }
}
