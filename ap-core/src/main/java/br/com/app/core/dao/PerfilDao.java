package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Perfil;
import br.com.app.core.repository.PerfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerfilDao extends GenericDao{

	@Autowired
	PerfilRepository perfilRepository;

	@Autowired
	Run configuracao;
	
	public void inserir(Perfil perfil){

		perfilRepository.save(perfil);
	}
	
	public List<Perfil> listar(){
		
		return perfilRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		perfilRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Perfil perfil) {

		perfilRepository.save(perfil);
		return false;
	}
	
	public Perfil buscarPorId(Long id) {

		Perfil perfil = perfilRepository.findDistinctById(id);
		return perfil;
	}

	
}
