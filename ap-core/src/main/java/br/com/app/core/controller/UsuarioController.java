package br.com.app.core.controller;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.app.core.model.Usuario;
import br.com.app.core.service.UsuarioService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@Api(value = "Municipio",  description = "API REST Municipio")
@RequestMapping("/usuario")
public class UsuarioController implements GenericController {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	UsuarioService usuarioService;

	@ApiOperation(value = "Insere um novo municipio")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> inserir(@RequestBody Usuario usuario){
				
		usuarioService.inserir(usuario);
		return new ResponseEntity<Usuario>(usuario, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna todos os municipios")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Usuario>> listar(){
		
		List<Usuario> listaConvidados = new ArrayList<>();
		listaConvidados = usuarioService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna o municipio pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Usuario> buscarPorId(@PathVariable("id") long id) {   

		Usuario usuario = new Usuario();
		usuario = usuarioService.buscarPorId(id);
		return new ResponseEntity<>(usuario, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais do municipio")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> alterar(@RequestBody Usuario usuario){
		
		usuarioService.alterar(usuario);
		return new ResponseEntity<>(usuario, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui um municipio")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> excluir(@RequestBody Usuario usuario){		
		
		usuarioService.excluir(usuario.getId());
		return new ResponseEntity<>(usuario, HttpStatus.CREATED);
	}


}
