package br.com.app.core.repository;

import br.com.app.core.model.Distribuidora;
import br.com.app.core.model.Regiao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RegiaoRepository extends JpaRepository<Regiao, Long>{

	@Query("select a from Regiao a where a.ativo = true")
	List<Regiao> findAllAtivo();

    Regiao findByNome(String nome);

	Regiao findDistinctById(Long id);

	void deleteById(Long id);
}
