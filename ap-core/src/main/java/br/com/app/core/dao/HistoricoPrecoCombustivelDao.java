package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Bandeira;
import br.com.app.core.model.HistoricoPrecoCombustivel;
import br.com.app.core.model.Municipio;
import br.com.app.core.repository.HistoricoPrecoCombustivelRepository;
import br.com.app.core.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoricoPrecoCombustivelDao extends GenericDao{

	@Autowired
	HistoricoPrecoCombustivelRepository historicoprecocombustivelRepository;

	@Autowired
	Run configuracao;

	public void inserir(HistoricoPrecoCombustivel historicoprecocombustivel){

		historicoprecocombustivelRepository.save(historicoprecocombustivel);
	}

	public List<HistoricoPrecoCombustivel> listar(){

		return historicoprecocombustivelRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {
		historicoprecocombustivelRepository.deleteById(id);
		return false;
	}

	public boolean alterar(HistoricoPrecoCombustivel historicoprecocombustivel) {
		historicoprecocombustivelRepository.save(historicoprecocombustivel);
		return false;
	}


	public HistoricoPrecoCombustivel buscarPorId(Long id) {
		return historicoprecocombustivelRepository.findHistoricoPrecoCombustivelById(id);
	}


    public List<MediaPrecoMunicipioResponse> mediaPrecoVenda(Municipio municipio) {
		return historicoprecocombustivelRepository.mediaPrecoMunicipio(municipio.getId());
    }


	public List<MediaPrecoRegiaoResponse> listaMediaPrecoRegiao() {
		return historicoprecocombustivelRepository.listaMediaPrecoRegiao();
	}

	public List<MediaPrecoDistribuidoraResponse> mediaPrecoVendaDistribuidora() {
		return historicoprecocombustivelRepository.listaMediaPrecoVendaDistribuidora();
	}

	public List<MediaPrecoDataColetaResponse> mediaPrecoDataColeta() {
		return historicoprecocombustivelRepository.mediaPrecoDataColeta();
	}

	public List<MediaPrecoMunicipioResponse> listaMediaPrecoMunicipio() {
		return historicoprecocombustivelRepository.listaMediaPrecoMunicipio();
	}

	public List<MediaPrecoBandeiraResponse> mediaPrecoBandeira(Bandeira bandeira) {
		return historicoprecocombustivelRepository.mediaPrecoBandeira(bandeira.getId());
	}

}
