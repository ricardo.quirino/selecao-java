package br.com.app.core.service;

import br.com.app.core.dao.ProdutoDao;
import br.com.app.core.model.Produto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdutoService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final GenericService genericService = new GenericService();

	@Autowired
	ProdutoDao produtoDao;

	public void inserir(Produto produto){

		produtoDao.inserir(produto);
	}

	public List<Produto> listar() {
		return produtoDao.listar();
	}

	public boolean excluir(Long id) {
		Produto produto = produtoDao.buscarPorId(id);
		produto.setAtivo(false);
		produtoDao.alterar(produto);
		return true;
	}

	public boolean alterar(Produto produto) {
		produtoDao.alterar(produto);
		return false;
	}

	public Produto buscarPorId(Long id) {
		return produtoDao.buscarPorId(id);
	}

	public Produto buscarPorNome(String nome) {
		return produtoDao.buscarPorNome(nome);
	}
}
