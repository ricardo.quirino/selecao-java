package br.com.app.core.controller;

import br.com.app.core.model.Produto;
import br.com.app.core.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "Produto",  description = "API REST Produto")
@RequestMapping("/produto")
public class ProdutoController implements GenericController {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	ProdutoService produtoService;


	@ApiOperation(value = "Insere um novo produto")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Produto> inserir(@RequestBody Produto produto){

		produtoService.inserir(produto);
		return new ResponseEntity<Produto>(produto, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna todos os produtos")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Produto>> listar(){

		List<Produto> listaConvidados = new ArrayList<>();
		listaConvidados = produtoService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna o produto pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Produto> buscarPorId(@PathVariable("id") long id) {

		Produto produto = new Produto();
		produto = produtoService.buscarPorId(id);
		return new ResponseEntity<>(produto, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais do produto")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Produto> alterar(@RequestBody Produto produto){

		produtoService.alterar(produto);
		return new ResponseEntity<>(produto, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui um produto")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Produto> excluir(@RequestBody Produto produto){

		produtoService.excluir(produto.getId());
		return new ResponseEntity<>(produto, HttpStatus.CREATED);
	}


}
