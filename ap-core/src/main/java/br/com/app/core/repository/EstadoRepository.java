package br.com.app.core.repository;

import br.com.app.core.model.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstadoRepository extends JpaRepository<Estado, Long>{

	@Query("select a from Estado a where a.ativo = true")
	List<Estado> findAllAtivo();

    Estado findByNome(String nome);

    Estado findDistinctById(Long id);

	void deleteById(Long id);
}
