package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Distribuidora;
import br.com.app.core.repository.DistribuidoraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistribuidoraDao extends GenericDao{

	@Autowired
	DistribuidoraRepository distribuidoraRepository;

	@Autowired
	Run configuracao;

	public void inserir(Distribuidora distribuidora){

		distribuidoraRepository.save(distribuidora);
	}

	public List<Distribuidora> listar(){

		return distribuidoraRepository.findAllAtivo();
	}

	public void excluir(Long id) {
		distribuidoraRepository.deleteById(id);
	}

	public boolean alterar(Distribuidora distribuidora) {

		distribuidoraRepository.save(distribuidora);
		return false;
	}

	public Distribuidora buscarPorId(Long id) {

		Distribuidora distribuidora = distribuidoraRepository.findDistinctById(id);
		return distribuidora;
	}

    public Distribuidora buscarPorCodigo(String codigo) {
		Distribuidora distribuidora = distribuidoraRepository.findByCodigo(codigo);
		return distribuidora;
    }
}
