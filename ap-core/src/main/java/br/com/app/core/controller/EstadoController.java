package br.com.app.core.controller;

import br.com.app.core.model.Estado;
import br.com.app.core.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "Estado",  description = "API REST Estado")
@RequestMapping("/estado")
public class EstadoController implements GenericController {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	EstadoService estadoService;


	@ApiOperation(value = "Insere um novo estado")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Estado> inserir(@RequestBody Estado estado){

		estadoService.inserir(estado);
		return new ResponseEntity<Estado>(estado, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna todos os estados")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Estado>> listar(){

		List<Estado> listaConvidados = new ArrayList<>();
		listaConvidados = estadoService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna o estado pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Estado> buscarPorId(@PathVariable("id") long id) {

		Estado estado = new Estado();
		estado = estadoService.buscarPorId(id);
		return new ResponseEntity<>(estado, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais do estado")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Estado> alterar(@RequestBody Estado estado){

		estadoService.alterar(estado);
		return new ResponseEntity<>(estado, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui um estado")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Estado> excluir(@RequestBody Estado estado){

		estadoService.excluir(estado.getId());
		return new ResponseEntity<>(estado, HttpStatus.CREATED);
	}


}
