package br.com.app.core.repository;

import java.util.List;
import java.util.Optional;

import br.com.app.core.model.Distribuidora;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.app.core.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{


	@Query("select a from Usuario a where a.ativo = true")
	List<Usuario> findAllAtivo();
	
	@Query("select a from Usuario a where a.pessoa.id = :id_pessoa")
	Usuario findByIdPessoa(@Param("id_pessoa") Long id_pessoa);

	Usuario findByLoginAndSenha(String login, String senha);

	Usuario findUsuarioByLogin(String login);

	Optional<Usuario> findByLogin(String login);

	Usuario findUsuarioById(final Long id);

	void deleteById(Long id);
}
