package br.com.app.core.repository;

import br.com.app.core.model.HistoricoPrecoCombustivel;
import br.com.app.core.response.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HistoricoPrecoCombustivelRepository extends JpaRepository<HistoricoPrecoCombustivel, Long>{


	HistoricoPrecoCombustivel findHistoricoPrecoCombustivelById(Long id);

	void deleteById(Long id);

	@Query("select a from HistoricoPrecoCombustivel a where a.ativo = true")
	List<HistoricoPrecoCombustivel> findAllAtivo();



	@Query("select new br.com.app.core.response.MediaPrecoMunicipioResponse(" +
			"h.municipio.nome, " +
			"h.municipio.estado.nome, " +
			"AVG(h.valorVenda)) " +
			"from HistoricoPrecoCombustivel h " +
			"where h.municipio.id = :id_municipio " +
			"group by h.municipio")
	List<MediaPrecoMunicipioResponse> mediaPrecoMunicipio(@Param("id_municipio") Long id_municipio);


	@Query("select new br.com.app.core.response.MediaPrecoRegiaoResponse(" +
			"h.municipio.estado.regiao.nome," +
			"AVG(h.valorCompra)," +
			"AVG(h.valorVenda)) " +
			"from HistoricoPrecoCombustivel h " +
			"group by h.municipio.estado.regiao.nome")
	List<MediaPrecoRegiaoResponse> listaMediaPrecoRegiao();


	@Query("select new br.com.app.core.response.MediaPrecoDistribuidoraResponse(" +
			"h.distribuidora.codigo," +
			"h.distribuidora.nome," +
			"h.municipio.nome," +
			"AVG(h.valorCompra)," +
			"AVG(h.valorVenda)) " +
			"from HistoricoPrecoCombustivel h " +
			"group by " +
			"h.distribuidora.codigo, " +
			"h.distribuidora.nome, " +
			"h.municipio.nome")
	List<MediaPrecoDistribuidoraResponse> listaMediaPrecoVendaDistribuidora();

	@Query("select new br.com.app.core.response.MediaPrecoDataColetaResponse(" +
			"h.dataColeta," +
			"AVG(h.valorCompra)," +
			"AVG(h.valorVenda)) " +
			"from HistoricoPrecoCombustivel h " +
			"group by " +
			"h.dataColeta")
	List<MediaPrecoDataColetaResponse> mediaPrecoDataColeta();


	@Query("select new br.com.app.core.response.MediaPrecoMunicipioResponse(" +
			"h.municipio.nome, " +
			"h.municipio.estado.nome, " +
			"AVG(h.valorVenda)) " +
			"from HistoricoPrecoCombustivel h " +
			"group by h.municipio")
	List<MediaPrecoMunicipioResponse> listaMediaPrecoMunicipio();


	@Query("select new br.com.app.core.response.MediaPrecoBandeiraResponse(" +
			"h.distribuidora.bandeira.nome, " +
			"AVG(h.valorCompra), " +
			"AVG(h.valorVenda)) " +
			"from HistoricoPrecoCombustivel h " +
			"where h.distribuidora.bandeira.id = :id_bandeira " +
			"group by h.distribuidora.bandeira.nome")
	List<MediaPrecoBandeiraResponse> mediaPrecoBandeira(@Param("id_bandeira") Long id_bandeira);
}
