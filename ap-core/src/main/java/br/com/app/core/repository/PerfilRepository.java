package br.com.app.core.repository;

import br.com.app.core.model.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PerfilRepository extends JpaRepository<Perfil, Long>{


	@Query("select a from Perfil a where a.ativo = true")
	List<Perfil> findAllAtivo();

	Perfil findDistinctById(Long id);

	void deleteById(Long id);
}
