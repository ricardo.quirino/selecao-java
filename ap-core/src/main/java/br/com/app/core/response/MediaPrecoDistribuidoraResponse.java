package br.com.app.core.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MediaPrecoDistribuidoraResponse implements Serializable {

  private String codigo;
  private String distribuidora;
  private String municipio;
  private Double mediaPrecoCompra;
  private Double mediaPrecoVenda;

  public MediaPrecoDistribuidoraResponse() {}


  public MediaPrecoDistribuidoraResponse(String codigo, String distribuidora, String municipio, Double mediaPrecoCompra, Double mediaPrecoVenda) {
    this.codigo = codigo;
    this.distribuidora = distribuidora;
    this.municipio = municipio;
    this.mediaPrecoCompra = mediaPrecoCompra;
    this.mediaPrecoVenda = mediaPrecoVenda;
  }
}
