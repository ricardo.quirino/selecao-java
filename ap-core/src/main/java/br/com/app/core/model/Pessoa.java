package br.com.app.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity(name="Pessoa")
public class Pessoa extends GenericModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id_pessoa")
	private Long id;
	
	@Column(name="Nome")
	private String nome;
	
	@Column(name="sobrenome")
	private String sobrenome;
	
	@Column(name="email")
	private String email;
	
	@Column(name="ativo")
	private Boolean ativo;


	public Pessoa() {}

	public Pessoa(String nome, String sobrenome, String email, Boolean ativo) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.email = email;
		this.ativo = ativo;
	}
}
