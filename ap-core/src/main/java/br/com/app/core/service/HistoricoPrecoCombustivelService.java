package br.com.app.core.service;

import br.com.app.core.dao.HistoricoPrecoCombustivelDao;
import br.com.app.core.model.*;
import br.com.app.core.response.*;
import br.com.app.core.to.FileUploadDTO;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class HistoricoPrecoCombustivelService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	@Autowired
	HistoricoPrecoCombustivelDao historicoPrecoCombustivelDao;

	@Autowired
	DistribuidoraService distribuidoraService;

	@Autowired
	ProdutoService produtoService;

	@Autowired
	MunicipioService municipioService;

	@Autowired
	EstadoService estadoService;

	@Autowired
	RegiaoService regiaoService;

	@Autowired
	BandeiraService bandeiraService;



	public void inserir(HistoricoPrecoCombustivel historicoprecocombustivel){

		historicoPrecoCombustivelDao.inserir(historicoprecocombustivel);
	}

	//todo
	public void inserirLote(MultipartFile fileupload){


		try {
			byte[] bytes = fileupload.getBytes();

			InputStream is = new ByteArrayInputStream(bytes);
			XSSFWorkbook workbook = new XSSFWorkbook(is);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIt = sheet.iterator();

			int i  = 0;
			List<FileUploadDTO> lista =  new ArrayList<>();

			while (rowIt.hasNext()) {
				FileUploadDTO file = null;

				if(i > 0){

					Row row =rowIt.next();
					String linha = row.getCell(0).toString();

					linha = linha.replace("  LTDA", " LTDA");
					linha = linha.replace("  ", ";");
					linha = linha.replace("  &", " &");

					file = new FileUploadDTO(linha.split(";"), linha);

					// Montar o objeto antes de inserir
					HistoricoPrecoCombustivel hpc = obterEntidade(file);

					inserir(hpc);
					lista.add(file);

				}else{
					rowIt.next();
				}

				i++;
				System.out.println(i + "/469802");
			}

			workbook.close();
			is.close();

		} catch(IOException e){
			e.printStackTrace();
		}

	}


	private HistoricoPrecoCombustivel obterEntidade(FileUploadDTO row){


		HistoricoPrecoCombustivel histPrecoCombut = new HistoricoPrecoCombustivel();

		//Obter a distribuitoda pelo codigo
		Distribuidora distribuidora = distribuidoraService.buscarPorCodigo(row.getCodigo());

		if(distribuidora != null){
			histPrecoCombut.setDistribuidora(distribuidora);
		}else{
			distribuidora = new Distribuidora();
			distribuidora.setAtivo(true);
			distribuidora.setCodigo(row.getCodigo());
			distribuidora.setNome(row.getDistribuidora());
			distribuidora.setMunicipio(obterMunicipio(row));
			distribuidora.setBandeira(obterBandeira(row));
			distribuidoraService.inserir(distribuidora);
		}

		//Obter produto pelo nome
		histPrecoCombut.setAtivo(true);
		histPrecoCombut.setDataColeta(row.getDataColeta());
		histPrecoCombut.setUnidadeMedida(row.getUnidadeMedida());
		histPrecoCombut.setValorCompra(row.getValorCompra());
		histPrecoCombut.setValorVenda(row.getValorVenda());
		histPrecoCombut.setDistribuidora(distribuidora);
		histPrecoCombut.setProduto(obterProduto(row));
		histPrecoCombut.setMunicipio(obterMunicipio(row));
		return histPrecoCombut;


	}

	private Municipio obterMunicipio(FileUploadDTO row){


		Municipio municipio = municipioService.buscarPorNome(row.getMunicipio());
		if(municipio != null){
			return municipio;
		}else{
			municipio = new Municipio();
			municipio.setAtivo(true);
			municipio.setNome(row.getMunicipio());
			municipio.setEstado(obterEstado(row));
			municipioService.inserir(municipio);
		}

		return municipio;

	}

	private Bandeira obterBandeira(FileUploadDTO row){

		Bandeira bandeira = bandeiraService.buscarPorNome(row.getBandeira());
		if(bandeira != null){
			return bandeira;
		}else{
			bandeira = new Bandeira();
			bandeira.setAtivo(true);
			bandeira.setNome(row.getBandeira());
			bandeiraService.inserir(bandeira);
		}

		return bandeira;
	}

	private Estado obterEstado(FileUploadDTO row) {

		Estado estado = estadoService.buscarPorNome(row.getSiglaEstado());
		if (estado != null) {
			return estado;
		} else {
			estado = new Estado();
			estado.setAtivo(true);
			estado.setNome(row.getSiglaEstado());
			estado.setRegiao(obterRegiao(row));
			estadoService.inserir(estado);
		}

		return estado;
	}

	private Regiao obterRegiao(FileUploadDTO row){
		Regiao regiao = regiaoService.buscarPorNome(row.getRegiao());
		if(regiao != null){
			return regiao;
		}else{
			regiao = new Regiao();
			regiao.setAtivo(true);
			regiao.setNome(row.getRegiao());
			regiaoService.inserir(regiao);
		}
		return regiao;
	}

	private Produto obterProduto(FileUploadDTO row){
		Produto produto = produtoService.buscarPorNome(row.getProduto());
		if(produto != null){
			return produto;
		}else{
			produto = new Produto();
			produto.setAtivo(true);
			produto.setNome(row.getProduto());
			produtoService.inserir(produto);
		}
		return produto;
	}

	public List<HistoricoPrecoCombustivel> listar() {
		return historicoPrecoCombustivelDao.listar();
	}

	public boolean excluir(Long id) {
		HistoricoPrecoCombustivel historicoprecocombustivel = historicoPrecoCombustivelDao.buscarPorId(id);
		historicoprecocombustivel.setAtivo(false);
		historicoPrecoCombustivelDao.alterar(historicoprecocombustivel);
		return true;
	}

	public boolean alterar(HistoricoPrecoCombustivel historicoprecocombustivel) {
		historicoPrecoCombustivelDao.alterar(historicoprecocombustivel);
		return false;
	}

	public HistoricoPrecoCombustivel buscarPorId(Long id) {
		return historicoPrecoCombustivelDao.buscarPorId(id);
	}

	public List<MediaPrecoMunicipioResponse> mediaPrecoVenda(String nome) {

		Municipio municipio = municipioService.buscarPorNome(nome);
		List<MediaPrecoMunicipioResponse> media = null;

		if(municipio != null){
			media = historicoPrecoCombustivelDao.mediaPrecoVenda(municipio);
		}
		return media;
	}

	public List<MediaPrecoRegiaoResponse> mediaPrecoPorRegiao() {
		return historicoPrecoCombustivelDao.listaMediaPrecoRegiao();
	}

	public List<MediaPrecoDistribuidoraResponse> mediaPrecoVendaDistribuidora() {
		return historicoPrecoCombustivelDao.mediaPrecoVendaDistribuidora();
	}

	public List<MediaPrecoDataColetaResponse> mediaPrecoDataColeta() {
		return historicoPrecoCombustivelDao.mediaPrecoDataColeta();
	}

	public List<MediaPrecoMunicipioResponse> listaMediaPrecoMunicipio() {
		return historicoPrecoCombustivelDao.listaMediaPrecoMunicipio();
	}

	public List<MediaPrecoBandeiraResponse> mediaPrecoBandeira(String nome) {

		Bandeira bandeira = bandeiraService.buscarPorNome(nome);
		List<MediaPrecoBandeiraResponse> media = null;

		if(bandeira != null){
			media = historicoPrecoCombustivelDao.mediaPrecoBandeira(bandeira);
		}
		return media;
	}


}
