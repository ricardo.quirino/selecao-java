package br.com.app.core.dao;

import br.com.app.core.Run;
import br.com.app.core.model.Regiao;
import br.com.app.core.repository.RegiaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegiaoDao extends GenericDao{

	@Autowired
	RegiaoRepository regiaoRepository;

	@Autowired
	Run configuracao;

	public void inserir(Regiao regiao){

		regiaoRepository.save(regiao);
	}

	public List<Regiao> listar(){

		return regiaoRepository.findAllAtivo();
	}

	public boolean excluir(Long id) {

		regiaoRepository.deleteById(id);
		return false;
	}

	public boolean alterar(Regiao regiao) {

		regiaoRepository.save(regiao);
		return false;
	}

	public Regiao buscarPorId(Long id) {

		Regiao regiao = regiaoRepository.findDistinctById(id);
		return regiao;
	}

    public Regiao buscarPorNome(String nome) {
		Regiao regiao = regiaoRepository.findByNome(nome);
		return regiao;
    }
}
