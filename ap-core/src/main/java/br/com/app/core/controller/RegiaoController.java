package br.com.app.core.controller;

import br.com.app.core.model.Regiao;
import br.com.app.core.response.MediaPrecoRegiaoResponse;
import br.com.app.core.service.RegiaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "Regiao",  description = "API REST Regiao")
@RequestMapping("/regiao")
public class RegiaoController implements GenericController {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	RegiaoService regiaoService;


	@ApiOperation(value = "Insere uma nova regiao")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Regiao> inserir(@RequestBody Regiao regiao){

		regiaoService.inserir(regiao);
		return new ResponseEntity<Regiao>(regiao, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna tadas as regiaos")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Regiao>> listar(){

		List<Regiao> listaConvidados = new ArrayList<>();
		listaConvidados = regiaoService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna a regiao pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Regiao> buscarPorId(@PathVariable("id") long id) {

		Regiao regiao = new Regiao();
		regiao = regiaoService.buscarPorId(id);
		return new ResponseEntity<>(regiao, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais da regiao")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Regiao> alterar(@RequestBody Regiao regiao){

		regiaoService.alterar(regiao);
		return new ResponseEntity<>(regiao, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui uma regiao")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Regiao> excluir(@RequestBody Regiao regiao){

		regiaoService.excluir(regiao.getId());
		return new ResponseEntity<>(regiao, HttpStatus.CREATED);
	}


	@ApiOperation(value = "Retorna a media de preço de combustivel por regiao")
	@RequestMapping(value="/mediaPreco", method = RequestMethod.GET)
	public ResponseEntity<List<MediaPrecoRegiaoResponse>> mediaPrecoVenda() {


		List<MediaPrecoRegiaoResponse> response = regiaoService.mediaPreco();

		if(response == null){
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		}else{
			return new ResponseEntity<>(response, HttpStatus.OK);
		}


	}


}
