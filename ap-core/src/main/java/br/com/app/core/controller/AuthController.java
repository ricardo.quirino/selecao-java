package br.com.app.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.app.core.model.Usuario;
import br.com.app.core.service.UsuarioService;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("/login")
//@CrossOrigin(maxAge = 10, origins = {"http://localhost:4200"})
public class AuthController implements GenericController {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Autowired
	//AuthService authService;
	
	@Autowired
	UsuarioService usuarioService;


	//INSERT POST

	@RequestMapping(method=RequestMethod.GET, value ="/{login}/{senha}" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> autenticarUsuario(@PathVariable("login") String login, @PathVariable("senha") String senha ){


		Usuario usuario = usuarioService.buscarPorLoginSenha(login, senha);

		if(usuario == null) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}else {
			return new ResponseEntity<Usuario>(usuario, HttpStatus.ACCEPTED);
		}

	}

	@RequestMapping(method=RequestMethod.GET, value ="/validarToken" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity validarToken(){
			return new ResponseEntity(HttpStatus.OK);
	}




}
