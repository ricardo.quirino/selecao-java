package br.com.app.core.controller;

import br.com.app.core.model.Perfil;
import br.com.app.core.service.PerfilService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "Perfil",  description = "API REST Perfil")
@RequestMapping("/perfil")
public class PerfilController implements GenericController {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	PerfilService perfilService;


	@ApiOperation(value = "Insere um novo perfil")
	@RequestMapping(method=RequestMethod.POST, value ="/inserir" , consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Perfil> inserir(@RequestBody Perfil perfil){

		perfilService.inserir(perfil);
		return new ResponseEntity<Perfil>(perfil, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retorna todos os perfis")
	@RequestMapping(method=RequestMethod.GET, value ="/listar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Perfil>> listar(){
		
		List<Perfil> listaConvidados = new ArrayList<>();
		listaConvidados = perfilService.listar();
		return new ResponseEntity<>(listaConvidados, HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna o perfil pelo {id}")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Perfil> buscarPorId(@PathVariable("id") long id) {

		Perfil perfil = new Perfil();
		perfil = perfilService.buscarPorId(id);
		return new ResponseEntity<>(perfil, HttpStatus.OK);
	}

	@ApiOperation(value = "Altera os dados cadastrais do perfil")
	@RequestMapping(method=RequestMethod.PUT, value ="/alterar" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Perfil> alterar(@RequestBody Perfil perfil){

		perfilService.alterar(perfil);
		return new ResponseEntity<>(perfil, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Exclui um perfil")
	@RequestMapping(method=RequestMethod.PUT, value ="/excluir" , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Perfil> excluir(@RequestBody Perfil perfil){
		
		perfilService.excluir(perfil.getId());
		return new ResponseEntity<>(perfil, HttpStatus.CREATED);
	}


}
