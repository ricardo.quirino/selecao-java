import { Estado } from "./estado";
export class Municipio {


    id: number;
    nome: string;
    ativo: boolean;
	estado: Estado;
}