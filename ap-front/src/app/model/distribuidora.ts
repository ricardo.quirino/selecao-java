import { Bandeira } from "./bandeira";
import { Municipio } from "./municipio";

export class Distribuidora {
  
    id: number;
    nome: string;
    ativo: boolean;
	codigo: string;
	municipio: Municipio;
	bandeira: Bandeira;
}