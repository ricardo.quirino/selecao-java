import { Regiao } from "./regiao";
export class Estado {
    
    id: number;
    nome: string;
    ativo: boolean;
	regiao: Regiao;
    
}