import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { AppCore } from 'src/app-core';
import { AuthService } from '../home/signin/auth.service';


@Injectable()
export class PessoaService{

    pessoaUrl = "";
    appCore: AppCore = new AppCore();
    

    constructor(private http: Http, private auth: AuthService){
        this.pessoaUrl = this.appCore.url+'/pessoa';
    }


    obterToken(): Headers{
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers
    }

    inserir(pessoa: any): Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.post(this.pessoaUrl+'/inserir', pessoa, {headers})
            .toPromise()
            .then(response => response.json());

    }

    alterar(pessoa: any):  Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.put(this.pessoaUrl+'/alterar', pessoa, {headers})
        .toPromise()
        .then(response => response.json());
                        
    }

    listar(): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.get(this.pessoaUrl+'/listar', {headers})
        .toPromise()
        .then(response => response.json());
    }
    
    excluir(pessoa: any): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.put(this.pessoaUrl+'/excluir', pessoa, {headers})
        .toPromise()
        .then(response => response.json());
      }

    buscarPorId(id: any): Promise<any> {
        const headers: Headers = this.obterToken();
        
        return this.http.get(`${this.pessoaUrl}/${id}`, { headers })
          .toPromise()
          .then(response => response.json());
    }

}