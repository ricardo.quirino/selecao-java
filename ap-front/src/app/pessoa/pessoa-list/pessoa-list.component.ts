import { Component, OnInit } from '@angular/core';
import { PessoaService } from '../pessoaService';
import { Pessoa } from '../Pessoa';


@Component({
  selector: 'app-pessoa-list',
  templateUrl: './pessoa-list.component.html',
  styleUrls: ['./pessoa-list.component.css']
})
export class PessoaListComponent implements OnInit {

  pessoas = [];
  pessoa = new Pessoa();

  constructor(private pessoaService: PessoaService){}

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.pessoaService.listar()
    .then(dados => {
      this.pessoas = dados;
    })
  }  

  prepararExcluir(pessoa: any){
    this.pessoa = pessoa;
    console.log(this.pessoa);
  }

  excluir(){
    this.pessoaService.excluir(this.pessoa)
    .then(dado =>{

      const index = this.pessoas.indexOf(this.pessoa, 0);
      if (index > -1) {
        this.pessoas.splice(index, 1);}

    })
  }

}
