import { Component, OnInit } from '@angular/core';
import { Pessoa } from '../Pessoa';
import { PessoaService } from '../pessoaService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pessoa-form',
  templateUrl: './pessoa-form.component.html',
  styleUrls: ['./pessoa-form.component.css']
})
export class PessoaFormComponent implements OnInit{

  pessoa = new Pessoa();
  pessoas = [];
  menssagemSucesso: boolean = false;
  menssagemErro: boolean = false;

  constructor(private pessoaService: PessoaService, private route: ActivatedRoute){}

  ngOnInit(){
    if(this.route.snapshot.params['id']){
      this.pessoaService.buscarPorId(this.route.snapshot.params['id'])
      .then(dado =>{
        this.pessoa = dado;
      })
    }
  }

  salvar(){
    
    if(this.pessoa.id){
      
      this.alterar();
    
    }else{
      this.inserir();
    }

    this.pessoa = new Pessoa();
  }

  inserir(){
    this.ativarPessoa();
    this.pessoaService.inserir(this.pessoa)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }


  alterar(){
    this.pessoaService.alterar(this.pessoa)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }

  buscarPorId(id: any){
    this.pessoaService.buscarPorId(id);
  }

  ativarPessoa(){
    this.pessoa.ativo = true;
  }

  ativarNotificacaoSucesso(){
    this.menssagemSucesso = true;
    this.menssagemErro = false;
  }

  ativarNotificacaoErro(){
    this.menssagemSucesso = false;
    this.menssagemErro = true;
  }



}
