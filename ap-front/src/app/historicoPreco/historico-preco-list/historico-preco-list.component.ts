import { Component, OnInit } from '@angular/core';
import { HistoricoPrecoService } from '../historicoPrecoService';
import { HistoricoPreco } from '../HistoricoPreco';


@Component({
  selector: 'app-historico-preco-list',
  templateUrl: './historico-preco-list.component.html',
  styleUrls: ['./historico-preco-list.component.css']
})
export class HistoricoPrecoListComponent implements OnInit {

  historicoPrecos = [];
  historicoPreco = new HistoricoPreco();

  constructor(private historicoPrecoService: HistoricoPrecoService){}

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.historicoPrecoService.listar()
    .then(dados => {
      this.historicoPrecos = dados;
    })
  }  

  prepararExcluir(historicoPreco: any){
    this.historicoPreco = historicoPreco;
    console.log(this.historicoPreco);
  }

  excluir(){
    this.historicoPrecoService.excluir(this.historicoPreco)
    .then(dado =>{

      const index = this.historicoPrecos.indexOf(this.historicoPreco, 0);
      if (index > -1) {
        this.historicoPrecos.splice(index, 1);}

    })
  }

}
