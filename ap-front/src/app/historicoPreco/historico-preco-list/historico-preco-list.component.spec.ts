import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoPrecoListComponent } from './historico-preco-list.component';

describe('HistoricoPrecoListComponent', () => {
  let component: HistoricoPrecoListComponent;
  let fixture: ComponentFixture<HistoricoPrecoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoPrecoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoPrecoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
