import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { AppCore } from 'src/app-core';
import { AuthService } from '../home/signin/auth.service';


@Injectable()
export class HistoricoPrecoService{

    historicoPrecoUrl = "";
    appCore: AppCore = new AppCore();
    

    constructor(private http: Http, private auth: AuthService){
        this.historicoPrecoUrl = this.appCore.url+'/historicoprecocombustivel';
    }


    obterToken(): Headers{
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers
    }

    inserir(historicoPreco: any): Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.post(this.historicoPrecoUrl+'/inserir', historicoPreco, {headers})
            .toPromise()
            .then(response => response.json());

    }

    alterar(historicoPreco: any):  Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.put(this.historicoPrecoUrl+'/alterar', historicoPreco, {headers})
        .toPromise()
        .then(response => response.json());
                        
    }

    listar(): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.get(this.historicoPrecoUrl+'/listar', {headers})
        .toPromise()
        .then(response => response.json());
    }
    
    excluir(historicoPreco: any): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.put(this.historicoPrecoUrl+'/excluir', historicoPreco, {headers})
        .toPromise()
        .then(response => response.json());
      }

    buscarPorId(id: any): Promise<any> {
        const headers: Headers = this.obterToken();
        
        return this.http.get(`${this.historicoPrecoUrl}/${id}`, { headers })
          .toPromise()
          .then(response => response.json());
    }
}