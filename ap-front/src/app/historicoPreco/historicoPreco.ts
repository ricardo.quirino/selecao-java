import { Distribuidora } from "../model/distribuidora";
import { Municipio } from "../model/municipio";
import { Produto } from "../model/produto";


export class HistoricoPreco {
	id: number;
	dataColeta: string;
	valorCompra: number;
	valorVenda: number
	unidadeMedida: string;
	ativo: boolean;
	distribuidora : Distribuidora;
	produto: Produto;
	municipio: Municipio;

}