import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoPrecoFormComponent } from './historico-preco-form.component';

describe('HistoricoPrecoFormComponent', () => {
  let component: HistoricoPrecoFormComponent;
  let fixture: ComponentFixture<HistoricoPrecoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoPrecoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoPrecoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
