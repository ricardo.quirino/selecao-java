import { Component, OnInit } from '@angular/core';
import { HistoricoPreco } from '../HistoricoPreco';
import { HistoricoPrecoService } from '../historicoPrecoService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-historico-preco-form',
  templateUrl: './historico-preco-form.component.html',
  styleUrls: ['./historico-preco-form.component.css']
})
export class HistoricoPrecoFormComponent implements OnInit{

  historicoPreco = new HistoricoPreco();
  historicoPrecos = [];
  menssagemSucesso: boolean = false;
  menssagemErro: boolean = false;


  constructor(private historicoPrecoService: HistoricoPrecoService, private route: ActivatedRoute){}

  ngOnInit(){
    if(this.route.snapshot.params['id']){
      this.historicoPrecoService.buscarPorId(this.route.snapshot.params['id'])
      .then(dado =>{
        this.historicoPreco = dado;
      })
    }

  }

  salvar(){
    
    if(this.historicoPreco.id){
      
      this.alterar();
    
    }else{
      this.inserir();
    }

    this.historicoPreco = new HistoricoPreco();
  }

  inserir(){
    this.ativarHistoricoPreco();
    this.historicoPrecoService.inserir(this.historicoPreco)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }


  alterar(){
    this.historicoPrecoService.alterar(this.historicoPreco)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }

  buscarPorId(id: any){
    this.historicoPrecoService.buscarPorId(id);
  }

  ativarHistoricoPreco(){
    this.historicoPreco.ativo = true;
  }

  ativarNotificacaoSucesso(){
    this.menssagemSucesso = true;
    this.menssagemErro = false;
  }

  ativarNotificacaoErro(){
    this.menssagemSucesso = false;
    this.menssagemErro = true;
  }



}
