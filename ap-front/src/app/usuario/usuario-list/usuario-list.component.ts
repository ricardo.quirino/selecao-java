import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuarioService';
import { Usuario } from '../Usuario';


@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.css']
})
export class UsuarioListComponent implements OnInit {

  usuarios = [];
  usuario = new Usuario();

  constructor(private usuarioService: UsuarioService){}

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.usuarioService.listar()
    .then(dados => {
      this.usuarios = dados;
    })
  }  

  prepararExcluir(usuario: any){
    this.usuario = usuario;
    console.log(this.usuario);
  }

  excluir(){
    this.usuarioService.excluir(this.usuario)
    .then(dado =>{
  

      const index = this.usuarios.indexOf(this.usuario, 0);
      if (index > -1) {
        this.usuarios.splice(index, 1);}

    })
  }

}
