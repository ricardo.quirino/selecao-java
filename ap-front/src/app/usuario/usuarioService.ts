import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { AppCore } from 'src/app-core';
import { AuthService } from '../home/signin/auth.service';


@Injectable()
export class UsuarioService{


    usuarioUrl = "";
    appCore: AppCore = new AppCore();
    

    constructor(private http: Http, private auth: AuthService){
        this.usuarioUrl = this.appCore.url+'/usuario';
    }


    obterToken(): Headers{
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers
    }

    inserir(usuario: any): Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.post(this.usuarioUrl+'/inserir', usuario, {headers})
            .toPromise()
            .then(response => response.json());

    }

    alterar(usuario: any):  Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.put(this.usuarioUrl+'/alterar', usuario, {headers})
        .toPromise()
        .then(response => response.json());
                        
    }

    listar(): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.get(this.usuarioUrl+'/listar', {headers})
        .toPromise()
        .then(response => response.json());
    }
    
    excluir(usuario: any): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.put(this.usuarioUrl+'/excluir', usuario, {headers})
        .toPromise()
        .then(response => response.json());
      }

    buscarPorId(id: any): Promise<any> {
        const headers: Headers = this.obterToken();
        
        return this.http.get(`${this.usuarioUrl}/${id}`, { headers })
          .toPromise()
          .then(response => response.json());
    }

}