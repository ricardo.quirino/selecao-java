import { Pessoa } from "../pessoa/Pessoa";
import { Perfil } from "../perfil/perfil";

export class Usuario {


     id: number;
     login: string;
     senha: string;
     pessoa: Pessoa;
     perfil: Perfil;
     ativo: boolean;
}

