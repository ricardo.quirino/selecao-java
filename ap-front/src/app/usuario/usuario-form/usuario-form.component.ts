import { Component, OnInit } from '@angular/core';
import { Usuario } from '../Usuario';
import { UsuarioService } from '../usuarioService';
import { ActivatedRoute } from '@angular/router';
import {SelectItem} from 'primeng/api';
import { PessoaService } from 'src/app/pessoa/pessoaService';
import { TouchSequence } from 'selenium-webdriver';
import { PerfilService } from 'src/app/perfil/perfilService';

@Component({
  selector: 'app-usuario-form',
  templateUrl: './usuario-form.component.html',
  styleUrls: ['./usuario-form.component.css']
})
export class UsuarioFormComponent implements OnInit{

  usuario = new Usuario();
  usuarios = [];
  pessoas = [];
  perfis = [];
  status: SelectItem[];
  menssagemSucesso: boolean = false;
  menssagemErro: boolean = false;

  constructor(private usuarioService: UsuarioService, private pessoaService: PessoaService,  private perfilService: PerfilService, private route: ActivatedRoute){

    this.status = [];
    this.status.push({label:'Ativo', value:true});
    this.status.push({label:'Inativo', value:false});
  }

  ngOnInit(){
    this.inicializarEdicao();
  }


  inicializarEdicao(){
    if(this.route.snapshot.params['id']){
      this.usuarioService.buscarPorId(this.route.snapshot.params['id'])
      .then(dado =>{
        this.usuario = dado;
        this.pessoas.push(this.usuario.pessoa);
        this.perfis.push(this.usuario.perfil);
      })

      this.perfilService.listar().then(dado =>{this.perfis = dado});
      const perfil = this.perfis.filter(perfil => perfil.id === this.usuario.perfil.id)[0];
      this.usuario.perfil = perfil;

      this.pessoaService.listar().then(dados2 =>{this.pessoas = dados2});
      const pessoa = this.pessoas.filter(pessoa => pessoa.id === this.usuario.pessoa.id)[0];
      this.usuario.pessoa = pessoa;
  
    }else{
      this.usuario.ativo = true;
      this.pessoaService.listar().then(dados =>{this.pessoas = dados});
      this.perfilService.listar().then(dados =>{this.perfis = dados});
    }
  }

  salvar(){
    if(this.usuario.id){
      
      this.alterar();
    
    }else{
      this.inserir();
    }

    this.usuario = new Usuario();
    this.usuario.ativo = true;
  }

  inserir(){
    this.usuarioService.inserir(this.usuario)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }


  alterar(){
    this.usuarioService.alterar(this.usuario)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }

  buscarPorId(id: any){
    this.usuarioService.buscarPorId(id);
  }


  ativarNotificacaoSucesso(){
    this.menssagemSucesso = true;
    this.menssagemErro = false;
  }

  ativarNotificacaoErro(){
    this.menssagemSucesso = false;
    this.menssagemErro = true;
  }



}
