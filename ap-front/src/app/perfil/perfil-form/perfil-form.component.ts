import { Component, OnInit } from '@angular/core';
import { Perfil } from '../Perfil';
import { PerfilService } from '../perfilService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-perfil-form',
  templateUrl: './perfil-form.component.html',
  styleUrls: ['./perfil-form.component.css']
})
export class PerfilFormComponent implements OnInit{

  perfil = new Perfil();
  perfils = [];
  menssagemSucesso: boolean = false;
  menssagemErro: boolean = false;


  constructor(private perfilService: PerfilService, private route: ActivatedRoute){}

  ngOnInit(){
    if(this.route.snapshot.params['id']){
      this.perfilService.buscarPorId(this.route.snapshot.params['id'])
      .then(dado =>{
        this.perfil = dado;
      })
    }

  }

  salvar(){
    
    if(this.perfil.id){
      
      this.alterar();
    
    }else{
      this.inserir();
    }

    this.perfil = new Perfil();
  }

  inserir(){
    this.ativarPerfil();
    this.perfilService.inserir(this.perfil)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }


  alterar(){
    this.perfilService.alterar(this.perfil)
    .then(dado =>{
      this.ativarNotificacaoSucesso();
    })
    .catch(dado =>{
      this.ativarNotificacaoErro();
    });
  }

  buscarPorId(id: any){
    this.perfilService.buscarPorId(id);
  }

  ativarPerfil(){
    this.perfil.ativo = true;
  }

  ativarNotificacaoSucesso(){
    this.menssagemSucesso = true;
    this.menssagemErro = false;
  }

  ativarNotificacaoErro(){
    this.menssagemSucesso = false;
    this.menssagemErro = true;
  }



}
