import { Component, OnInit } from '@angular/core';
import { PerfilService } from '../perfilService';
import { Perfil } from '../Perfil';


@Component({
  selector: 'app-perfil-list',
  templateUrl: './perfil-list.component.html',
  styleUrls: ['./perfil-list.component.css']
})
export class PerfilListComponent implements OnInit {

  perfils = [];
  perfil = new Perfil();

  constructor(private perfilService: PerfilService){}

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.perfilService.listar()
    .then(dados => {
      this.perfils = dados;
    })
  }  

  prepararExcluir(perfil: any){
    this.perfil = perfil;
    console.log(this.perfil);
  }

  excluir(){
    this.perfilService.excluir(this.perfil)
    .then(dado =>{

      const index = this.perfils.indexOf(this.perfil, 0);
      if (index > -1) {
        this.perfils.splice(index, 1);}

    })
  }

}
