import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { AppCore } from 'src/app-core';
import { AuthService } from '../home/signin/auth.service';


@Injectable()
export class PerfilService{

    perfilUrl = "";
    appCore: AppCore = new AppCore();
    

    constructor(private http: Http, private auth: AuthService){
        this.perfilUrl = this.appCore.url+'/perfil';
    }


    obterToken(): Headers{
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers
    }

    inserir(perfil: any): Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.post(this.perfilUrl+'/inserir', perfil, {headers})
            .toPromise()
            .then(response => response.json());

    }

    alterar(perfil: any):  Promise<any>{
        const headers: Headers = this.obterToken();

        return this.http.put(this.perfilUrl+'/alterar', perfil, {headers})
        .toPromise()
        .then(response => response.json());
                        
    }

    listar(): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.get(this.perfilUrl+'/listar', {headers})
        .toPromise()
        .then(response => response.json());
    }
    
    excluir(perfil: any): Promise<any> {
        const headers: Headers = this.obterToken();

        return this.http.put(this.perfilUrl+'/excluir', perfil, {headers})
        .toPromise()
        .then(response => response.json());
      }

    buscarPorId(id: any): Promise<any> {
        const headers: Headers = this.obterToken();
        
        return this.http.get(`${this.perfilUrl}/${id}`, { headers })
          .toPromise()
          .then(response => response.json());
    }
}