import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  teste = false;
  title = 'app-front';

  usuarioLogado = false;

  constructor(private router: Router){

  }

  ngOnInit(){
    
  }

  exibindoMenus(){
    return this.router.url !== '/'
  }
}
