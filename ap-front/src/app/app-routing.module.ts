import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { UsuarioFormComponent } from './usuario/usuario-form/usuario-form.component';
import { UsuarioListComponent } from './usuario/usuario-list/usuario-list.component';
import { PessoaFormComponent } from './pessoa/pessoa-form/pessoa-form.component';
import { PessoaListComponent } from './pessoa/pessoa-list/pessoa-list.component';
import { SigninComponent } from './home/signin/signin.component';
import { IndexComponent } from './home/index/index.component';
import { PerfilListComponent } from './perfil/perfil-list/perfil-list.component';
import { PerfilFormComponent } from './perfil/perfil-form/perfil-form.component';
import { HistoricoPrecoFormComponent } from './historicoPreco/historico-preco-form/historico-preco-form.component';
import { HistoricoPrecoListComponent } from './historicoPreco/historico-preco-list/historico-preco-list.component';

const routes: Routes = [
  { path: '', component: SigninComponent},

  { path: 'usuario/add', component: UsuarioFormComponent},
  { path: 'usuario/lista', component: UsuarioListComponent},
  { path: 'usuario/:id', component: UsuarioFormComponent},

  { path: 'pessoa/add', component: PessoaFormComponent},
  { path: 'pessoa/lista', component: PessoaListComponent},
  { path: 'pessoa/:id', component: PessoaFormComponent},

  { path: 'perfil/add', component: PerfilFormComponent},
  { path: 'perfil/lista', component: PerfilListComponent},
  { path: 'perfil/:id', component: PerfilFormComponent},

  { path: 'historico-preco/add', component: HistoricoPrecoFormComponent},
  { path: 'historico-preco/lista', component: HistoricoPrecoListComponent},
  { path: 'historico-preco/:id', component: HistoricoPrecoFormComponent},

  { path: 'index', component: IndexComponent},
  { path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),],
  exports: [RouterModule]
})
export class AppRoutingModule { }
