import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SigninComponent } from './signin/signin.component';
import { JwtHelper } from 'angular2-jwt';
import { IndexComponent } from './index/index.component';
@NgModule({
  declarations: [SigninComponent, IndexComponent],
  imports: [
    BrowserModule,
    FormsModule,

  ],
  providers:[
    JwtHelper
  ]
  
})
export class HomeModule { }
