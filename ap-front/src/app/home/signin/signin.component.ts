import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
@Component({
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  usuarioLogado = false;

  usuario: string;
  senha: string;
  
  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  login(){
    this.auth.login(this.usuario, this.senha);
  }
}
