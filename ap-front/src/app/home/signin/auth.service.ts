import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Usuario } from 'src/app/usuario/Usuario';
import { JwtHelper } from 'angular2-jwt';
import { Router } from '@angular/router';
import { AppCore } from 'src/app-core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  oauthTokenUrl = 'http://localhost:8081/auth/login'
  usuario: Usuario = new Usuario();
  jwtPayload: any;
  token: string = "";
  appCore: AppCore = new AppCore();
  loginUrl = "";

  constructor(private http: Http, private jwtHelper: JwtHelper, private router: Router ) { 
    this.loginUrl = this.appCore.url+'/login';
  }

  obterHeaders(): Headers{
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return headers
}

  //Metodo LOGIN
  login(login: string, senha: string){
    

    const headers: Headers = this.obterHeaders();

    return this.http.get(`${this.loginUrl}/${login}/${senha}`, { headers })
      .toPromise()
      .then(dados => {
        this.router.navigate(['/index']);
      });
  }

}
