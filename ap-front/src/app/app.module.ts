import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorsModule } from './errors/errors.module';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import {TableModule} from 'primeng/table';
import {SelectButtonModule} from 'primeng/selectbutton';
import {MultiSelectModule} from 'primeng/multiselect';

import { UsuarioFormComponent } from './usuario/usuario-form/usuario-form.component';
import { UsuarioListComponent } from './usuario/usuario-list/usuario-list.component';
import { PessoaFormComponent } from './pessoa/pessoa-form/pessoa-form.component';
import { PessoaListComponent } from './pessoa/pessoa-list/pessoa-list.component';
import { PessoaService } from './pessoa/pessoaService';
import { UsuarioService } from './usuario/usuarioService';
import { HomeModule } from './home/home.module';
import { MenuLeftComponent } from './home/menu-left/menu-left.component';
import { MenuTopComponent } from './home/menu-top/menu-top.component';
import { FooterComponent } from './home/footer/footer.component';
import { AuthService } from './home/signin/auth.service';
import { JwtHelper } from 'angular2-jwt';
import { PerfilFormComponent } from './perfil/perfil-form/perfil-form.component';
import { PerfilListComponent } from './perfil/perfil-list/perfil-list.component';
import { PerfilService } from './perfil/perfilService';
import { HistoricoPrecoFormComponent } from './historicoPreco/historico-preco-form/historico-preco-form.component';
import { HistoricoPrecoListComponent } from './historicoPreco/historico-preco-list/historico-preco-list.component';
import { HistoricoPrecoService } from './historicoPreco/historicoPrecoService';

@NgModule({
  declarations: [
    AppComponent,
    UsuarioFormComponent,
    UsuarioListComponent,
    PessoaFormComponent,
    PessoaListComponent,
    MenuLeftComponent,
    MenuTopComponent,
    FooterComponent,
    PerfilFormComponent,
    PerfilListComponent,
    HistoricoPrecoFormComponent,
    HistoricoPrecoListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ErrorsModule,
    FormsModule,
    HttpModule,
    TableModule,
    SelectButtonModule,
    MultiSelectModule,
    HomeModule,
  ],
  providers: [PessoaService, UsuarioService, PerfilService, AuthService, JwtHelper, HistoricoPrecoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
