# 1) Projeto ap-front (FRONTEND)
O projeto frontend foi desenvolvido com o objetivo de atender os requisitos da seleção para desenvolvedor java. 

## Foi usado as seguintes tecnologias para o desenvolvimento: 

* Angular  7.0
* Webpack com Angular CLI
* Bootstrap (Tema AdminLTE-2.3.0)

## Iniciar a aplicação:

* Baixar projeto: 				       git https://gitlab.com/ricardo.quirino/selecao-java.git
* Entrar na pasta do projeto: 	       cd/selecao-java/ap-front
* Baixar as dependencias do projeto :  npm install --dev
* Iniciar a aplicação: 			       ng server
* Acessar:						       http://localhost:4200/


############################################################################################################################



# 2) Projeto ap-core (BACKEND)
O projeto API REST foi desenvolvido com o objetivo de atender os requisitos da seleção para desenvolvedor java. 
## Foi usado as seguintes tecnologias para o desenvolvimento: 

* Java 8
* Springo boot 2.5
* lombok 1.16
* banco h2
* swagger
* JPA

A api esta configurada para iniciar na 
porta: 8081 
url: http://localhost:8081


## Acessos:
Ao iniciar a aplicação um usuario de acesso será criado para ser usado como admin do sistema 
Após iniciar as duas aplicaçõe (ap-front e ap-core) use estas credencias para entrar na aplicação: http://localhost:4200/

### Credenciais 
login: user.admin
senha: 1234


## Banco H2:
H2 Console:
http://localhost:8081/h2/login.do


# 3) Requisições HTTP REST 

## Swagger documentação interativa :
http://localhost:8081/swagger-ui.html


## Visual Studio Code Rest Client:
Arquivos na pasta: /rest-client-aip

# 4) Importação do arquivo: 
* baixar o arquivo :http://www.anp.gov.br/images/dadosabertos/precos/2018-1_CA.csv
* importar usando a api: http://localhost:8081/swagger-ui.html#/historico-preco-combustivel-controller/singleFileUploadUsingPOST

Obs: a aplicação esta configurada para receber arquivo de até 60MB


### API Solicitadas: request-api-historico-preco e request-api-usuario

Obs: A requisições prontas das API's a baixa esta no arquivio 

* Recurso para CRUD de usuários
* Recurso para CRUD de histórico de preço de combustível
* Recurso para importação de csv
* Recurso que retorne a média de preço de combustível com base no nome do município
* Recurso que retorne todas as informações importadas por sigla da região
* Recurso que retorne os dados agrupados por distribuidora
* Recurso que retorne os dados agrupados pela data da coleta
* Recurso que retorne o valor médio do valor da compra e do valor da venda por município
* Recurso que retorne o valor médio do valor da compra e do valor da venda por bandeira